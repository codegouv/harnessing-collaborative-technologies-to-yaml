# harnessing-collaborative-technologies-to-yaml

Convert Harnessing Collaborative Technologies report web site to a Git repository of YAML files.


## Usage

```bash
./harvest_html.py html/
./html_to_yaml.py html/ ../harnessing-collaborative-technologies-yaml/
```
