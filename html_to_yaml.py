#! /usr/bin/env python3


# harnessing-collaborative-technologies-to-yaml -- Convert Harnessing Collaborative Technologies report web site to a Git repository of YAML files
# By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
#
# Copyright (C) 2016 Etalab
# https://git.framasoft.org/codegouv/harnessing-collaborative-technologies-to-yaml
#
# harnessing-collaborative-technologies-to-yaml is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# harnessing-collaborative-technologies-to-yaml is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http:>www.gnu.org/licenses/>.


import argparse
import collections
import os
import sys
import urllib.parse

import lxml.html
from slugify import slugify
import yaml


# YAML configuration


class folded_str(str):
    pass


class literal_str(str):
    pass


def ordered_dict_representer(dumper, data):
    return dumper.represent_mapping('tag:yaml.org,2002:map', data.items())


yaml.add_representer(folded_str, lambda dumper, data: dumper.represent_scalar(u'tag:yaml.org,2002:str',
    data, style='>'))
yaml.add_representer(literal_str, lambda dumper, data: dumper.represent_scalar(u'tag:yaml.org,2002:str',
    data, style='|'))
yaml.add_representer(dict, ordered_dict_representer)
yaml.add_representer(collections.OrderedDict, ordered_dict_representer)
yaml.add_representer(str, lambda dumper, data: dumper.represent_scalar(u'tag:yaml.org,2002:str', data))


#


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('pages_dir', help = 'path of source directory containing harvested HTML pages')
    parser.add_argument('yaml_dir', help = 'path of target directory for harvested YAML files')
    args = parser.parse_args()

    assert os.path.exists(args.pages_dir)
    if not os.path.exists(args.yaml_dir):
        os.makedirs(args.yaml_dir)

    site_url = "http://collaboration.grantcraft.org/"
    for filename in os.listdir(args.pages_dir):
        if not filename.endswith('.html'):
            continue
        entry_id = int(filename.split('.')[0])
        file_path = os.path.join(args.pages_dir, filename)
        with open(file_path, encoding = 'utf-8') as html_file:
            html = html_file.read()
            html_element = lxml.html.document_fromstring(html)

            category = html_element.xpath('//div[@class="tools-category-title tid-{}"]/h3'.format(entry_id))[0].text.strip()
            print('\n*** ', category)
            for article in html_element.xpath('//div[@class="tools-wrap"]/article'):
                title = article.find('header/h2').text.strip()
                plan = article.find('header/em[@class="plan"]').text.strip()
                div = article.find('header/div[@class="tool-right"]')
                ideal = [
                    li.text.strip()
                    for li in div.iterfind('div[@class="ideal-b"]/ul/li')
                    if 'active' in li.attrib['class'].split()
                    ]
                for ease_div in div.iterfind('div'):
                    if 'ease-b' in ease_div.attrib['class'].split():
                        ease = ease_div.find('p').text.strip()
                        break
                else:
                    ease = None
                mobile = div.find('div[@class="mobile-b"]/p').attrib['class'].strip() == 'active'
                description_element = article.find('div[@class="tool-content"]')
                content = article.find('div[@class="tool-content"]')
                logo_url = urllib.parse.urljoin(site_url, content.find('p[1]/img').attrib['src'])
                description_element = content.find('p[2]')
                if description_element is None:
                    description_element = content.find('p[1]')
                    description_element.remove(description_element.find('img'))  # Remove image.
                for element in description_element.xpath('.//*'):
                    src = element.attrib.get('src')
                    if src is not None:
                        element.attrib['src'] = urllib.parse.urljoin(site_url, src)
                description = lxml.html.tostring(description_element, encoding = 'unicode', with_tail = False) \
                    .strip().split('>', 1)[1].rsplit('<', 1)[0].strip()
                while description.startswith('<br>'):
                    description = description.lstrip('<br>').lstrip()
                description = description.lstrip('“').rstrip('”').strip()
                site_url = content.find('a[@class="visit-site"]').attrib['href'].strip()

                tool = dict(
                    category = category,
                    description = description,
                    ease = ease,
                    ideal = ideal,
                    logo_url = logo_url,
                    mobile = mobile,
                    plan = plan,
                    site_url = site_url,
                    title = title,
                    )

                slug = slugify(title)
                with open(os.path.join(args.yaml_dir, '{}.yaml'.format(slug)), 'w') as yaml_file:
                    yaml.dump(tool, yaml_file, allow_unicode = True, default_flow_style = False,
                        indent = 2, width = 120)

    return 0


if __name__ == '__main__':
    sys.exit(main())
