#! /usr/bin/env python3


# harnessing-collaborative-technologies-to-yaml -- Convert Harnessing Collaborative Technologies report web site to a Git repository of YAML files
# By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
#
# Copyright (C) 2016 Etalab
# https://git.framasoft.org/codegouv/harnessing-collaborative-technologies-to-yaml
#
# harnessing-collaborative-technologies-to-yaml is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# harnessing-collaborative-technologies-to-yaml is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http:>www.gnu.org/licenses/>.


"""Harvest HTML pages from Harnessing Collaborative Technologies report."""


import argparse
import os
import sys
import urllib.parse
import urllib.request

import lxml.html


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('pages_dir', help = 'path of target directory for harvested HTML pages')
    args = parser.parse_args()

    if not os.path.exists(args.pages_dir):
        os.makedirs(args.pages_dir)

    site_url = "http://collaboration.grantcraft.org/"
    response = urllib.request.urlopen(site_url)
    html = response.read().decode("utf-8")
    html_element = lxml.html.document_fromstring(html)
    a_elements = html_element.xpath('//ul[@class="menu"]/li/a[@class="menu-item-url"]')
    paths = set(
        a_element.attrib['href']
        for a_element in a_elements
        )

    for path in paths:
        page_url = urllib.parse.urljoin(site_url, path)
        response = urllib.request.urlopen(page_url)
        html = response.read().decode("utf-8")
        project_number = path.split('/')[-1]
        page_path = os.path.join(args.pages_dir, '{}.html'.format(project_number))
        with open(page_path, 'w', encoding = 'utf-8') as page_file:
            page_file.write(html)

    return 0


if __name__ == '__main__':
    sys.exit(main())
